## [1.5.3](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.5.2...1.5.3) (2025-02-28)


### Bug Fixes

* **deps:** update dependency cyclonedx-python-lib to v9 ([b6003dc](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/b6003dc893109fda7d9f6494b41f28883582e84f))

## [1.5.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.5.1...1.5.2) (2025-02-28)


### Bug Fixes

* **deps:** update poetry dependencies ([b28c817](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/b28c81716cf53d8bb7c7d8d9e1631c46560cb156))

## [1.5.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.5.0...1.5.1) (2025-02-14)


### Bug Fixes

* **deps:** update poetry dependencies ([9917f80](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/9917f8070d71d224c0a34c26be84d00d37a0b64f))

# [1.5.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.9...1.5.0) (2025-02-12)


### Features

* -t/--tags to add tags of project to create ([e4371cf](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/e4371cf718360a2b552a725a16ceee750c34b41f))

## [1.4.9](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.8...1.4.9) (2025-02-07)


### Bug Fixes

* **deps:** update poetry dependencies ([36c0e49](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/36c0e492a5db9b7e94cb486f81e582d958dfe09a))

## [1.4.8](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.7...1.4.8) (2025-01-31)


### Bug Fixes

* **deps:** update poetry dependencies ([25c42ab](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/25c42ab42cdc9634471cbb1b65a1fe26c6779ab4))

## [1.4.7](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.6...1.4.7) (2024-11-25)


### Bug Fixes

* support latest cdxgen sbom ([16fd396](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/16fd396bd6e56abe5b6a5b76532376f70238bb79))

## [1.4.6](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.5...1.4.6) (2024-10-11)


### Bug Fixes

* **deps:** update poetry dependencies ([628596d](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/628596d3ff3faa9b945171537790b7e060d347e2))

## [1.4.5](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.4...1.4.5) (2024-10-02)


### Bug Fixes

* add a better error message when server is unreachable ([550816b](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/550816b039f54c77b81503ac8f2a85d4ab49833f))
* check if the token is valid ([821d106](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/821d10691ecfdc7bc3a2bb7c9037a04209b289b7))

## [1.4.4](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.3...1.4.4) (2024-09-20)


### Bug Fixes

* **deps:** update poetry dependencies ([9340b97](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/9340b97e8c4a18c875dd12a495b51b1b11262506))

## [1.4.3](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.2...1.4.3) (2024-08-16)


### Bug Fixes

* **deps:** update poetry dependencies ([55a3053](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/55a3053f192019e5f957b1ba9201ed568781119f))

## [1.4.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.1...1.4.2) (2024-07-30)


### Bug Fixes

* support duplicated component with different bom-ref ([9b6df3e](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/9b6df3edf9f9e4f42d75446d96cf289542866e59))

## [1.4.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.4.0...1.4.1) (2024-07-25)


### Bug Fixes

* quote purl params before trim ([0141fbd](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/0141fbd04e15f4b222c956576bcf2a6df23f298f))

# [1.4.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.3.4...1.4.0) (2024-07-13)


### Features

* add Quality Gate support ([0cf502e](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/0cf502ed4612f44f2594be3382654752c2d06b26))

## [1.3.4](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.3.3...1.3.4) (2024-07-12)


### Bug Fixes

* **deps:** update poetry dependencies ([3f0847b](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/3f0847b9833a3496c95d94533b4f33bb173a6f00))

## [1.3.3](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.3.2...1.3.3) (2024-07-05)


### Bug Fixes

* **deps:** update dependency cyclonedx-python-lib to v7.5.0 ([8353409](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/83534091cf34dc608c9ee515f9a1b6ead95bd8be))

## [1.3.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.3.1...1.3.2) (2024-07-04)


### Bug Fixes

* accept other arguments ([ad32c59](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/ad32c59292e9364432b9192c76a0a85b7b1ca26a))
* support old bom/token path ([ce27817](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/ce27817ec832bd6dd73a36fc77d73a0c68efb35b))

## [1.3.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.3.0...1.3.1) (2024-07-02)


### Bug Fixes

* argparse.Namespace is not a dict ([d106caf](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/d106caf6abe01bcf202190c25acbe273c7f9ae02))

# [1.3.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.2.4...1.3.0) (2024-07-01)


### Features

* show findings ([5350c38](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/5350c38d59a83f8f3cbf25b04a50d05062d12cb0))

## [1.2.4](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.2.3...1.2.4) (2024-06-30)


### Bug Fixes

* accept recursive glob ([57c75d4](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/57c75d4d15478eb2b8e186457569db9ac684447e))

## [1.2.3](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.2.2...1.2.3) (2024-06-17)


### Bug Fixes

* **merge:** create output report parent directory if need be ([f71450e](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/f71450e65e40b225ac5523ea6a47444628ee5754))

## [1.2.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.2.1...1.2.2) (2024-06-15)


### Bug Fixes

* **version:** own Version impl to compare DT server version ([3371caf](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/3371caf149a6d96cdb4519766d8d230ca07b5c9a)), closes [#1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/issues/1)

## [1.2.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.2.0...1.2.1) (2024-06-14)


### Bug Fixes

* **deps:** update dependency cyclonedx-python-lib to v7.4.1 ([222b4fb](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/222b4fb73db96202b797e096b417cc9fbde48f44))

# [1.2.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.1.1...1.2.0) (2024-06-12)


### Features

* add SBOM merge support ([86ebf6d](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/86ebf6d158dad2ba433375dc48212ac975c7896c))

## [1.1.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.1.0...1.1.1) (2024-05-31)


### Bug Fixes

* **deps:** update poetry dependencies ([6c5dce0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/6c5dce0145ff754f3ba13188a17e3308018e561f))

# [1.1.0](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.2...1.1.0) (2024-05-29)


### Features

* support SBOM metadata expressions in project path ([2579e70](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/2579e70813390ce030f765046ba74975bae9be2f))

## [1.0.2](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.1...1.0.2) (2024-05-27)


### Bug Fixes

* improve logs and error management ([6c375df](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/6c375df48d27036b01d0ca6b1bc481d3dbf23808))

## [1.0.1](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/compare/1.0.0...1.0.1) (2024-05-26)


### Bug Fixes

* manage projects creation ([8049980](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/8049980206330d2faf8f9091b2c4308ecf51faa4))

# 1.0.0 (2024-05-25)


### Features

* initial commit ([5471a06](https://gitlab.com/to-be-continuous/tools/dt-sbom-scanner/commit/5471a06ee63671c752be98e434e96f0d5a061629))
