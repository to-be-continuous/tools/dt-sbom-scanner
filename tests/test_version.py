import pytest

from sbom_scanner.scan import Version


def test_parse_simple():
    v = Version("1.2.3")
    assert v.major == 1
    assert v.minor == 2
    assert v.patch == 3


def test_parse_w_prerelease():
    v = Version("1.2.3-beta4")
    assert v.major == 1
    assert v.minor == 2
    assert v.patch == 3
    assert v.prerelease == "beta4"


def test_parse_w_build():
    v = Version("1.2.3+linux")
    assert v.major == 1
    assert v.minor == 2
    assert v.patch == 3
    assert v.build == "linux"


def test_parse_w_prerelease_and_build():
    v = Version("1.2.3-beta4+linux")
    assert v.major == 1
    assert v.minor == 2
    assert v.patch == 3
    assert v.prerelease == "beta4"
    assert v.build == "linux"


def test_parse_fail():
    with pytest.raises(ValueError):
        # bad version string
        Version("not.a.version")


def test_lt_1():
    v1 = Version("1.2.3")
    v2 = Version("1.2.4")
    assert v1 < v2


def test_lt_2():
    v1 = Version("1.2.3")
    v2 = Version("1.3.0")
    assert v1 < v2


def test_lt_3():
    v1 = Version("1.2.3")
    v2 = Version("2.0.0")
    assert v1 < v2


def test_lt_4():
    v1 = Version("1.2.3-beta1")
    v2 = Version("1.2.3-beta2")
    assert v1 < v2


def test_lt_5():
    v1 = Version("1.2.3-alpha1")
    v2 = Version("1.2.3-beta1")
    assert v1 < v2


def test_eq():
    v1 = Version("1.2.3-beta1")
    v2 = Version("1.2.3-beta1")
    assert v1._compare(v2) == 0
    assert v1 == v2
