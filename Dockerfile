FROM registry.hub.docker.com/library/python:3.13-alpine

WORKDIR /code

COPY ./dist/*.whl /code/

# hadolint ignore=DL3018
RUN apk upgrade --no-cache \
    && apk add git --no-cache \
    && pip install --no-cache-dir /code/*.whl

ENTRYPOINT [ "sbom-scanner" ]
CMD [ "." ]